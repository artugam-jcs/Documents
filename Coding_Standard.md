> Latest: https://docs.google.com/document/d/1tB_wyJ2hwUoXyNvGBKhxkKVyc7O4aaMUZgWJu6OR42M/edit

# Coding Standards & Practices for GAP

## Motivation

To provide a set of standards for how code is written within the GAP program. As a
professional software engineer, you will need to conform to the standards set forth by the
company you work with. These standards are based on common professional practices

## Naming Conventions

Classes, namespaces, functions, `typedef`’s and enum names should be named using
CamelCase, where the first letter is capitalized:

```cpp
class FooBar;
namespace FooBar { }
enum FooBar { };
typedef std::vector<int> IntVector;
void DoTheThing();
```

Filenames should be named in the same way:

```
FooBar.h
FooBar.cpp
```

Variables should be named in camel case, but with the first letter lowercase:

```cpp
int fooBar;
```

Member variables, global variables, and static variables must have a single letter prefix with an
underscore denoting their scope. Local variables have no prefix.

```cpp
int m_fooBar; // member variable
int g_fooBar; // global variable
int s_fooBar; // static variable (class or non­class)
int fooBar; // local variable
```

Additionally, pointers should be prefixed with a ‘p’:

```cpp
int* m_pFooBar; // member pointer variable
int* g_pFooBar; // global pointer variable
int* s_pFooBar; // static pointer variable (class or non­class)
int* pFooBar; // local pointer variable
```

Constant variables should be prefixed with a k. This includes const variables and enum values:

```cpp
const char* k_fooBar = “baz”;
enum FooBar
{
    k_fooBar,
    k_fooBarBaz
};
```

`#define`’s should be all caps, with underscores between each word:

```cpp
#define FOO_BAR
```

## Bracing & Formatting

Bracing & indentation should adhere to the Allman style:
https://en.wikipedia.org/wiki/Indent_style#Allman_style

```cpp
while (x == y)
{
    Foo();
    FooBar();
}
FooBarBaz();
```

Indentation should be set to 4 spaces. Make sure you are inserting spaces rather than inserting
the tab character. In Visual Studio 2013, you can set this by going to Tools ­> Options ­> Text
Editor ­> C/C++ ­> Tabs. Change the Tab size to 4 and make sure “Insert spaces” is set

Binary operators should always be surrounded by a single space on either end, while unary
operators should not have any spaces:

```cpp
int foo = bar + baz;
++foo;
```

Never use single­line if statements or loops:

```cpp
if (foo) FooBar(); // NO

while (foo) FooBar(); // NO
```

Instead, put it on the next line:


```cpp
if (foo)
    FooBar();

while (foo)
    FooBar();
```

Multiple statements on the same line are also not allowed:

```cpp
x = 10; y = 15; // NO
```

Single­line functions are allowed only if they are trivial accessors or mutators:

```cpp
class Foo
{
    int m_bar;
public:
    int GetBar() const { return m_bar; }
    void SetBar(int bar) { m_bar = bar; }

    // NO. This function has two statements, so it can’t be
    // a single line. It’s also no longer a trivial getter
    // since it has additional side effects.
    int IncrementAndGetBar() { ++m_bar; return m_bar; }
};
```

It’s okay to omit the braces on an if statement or loop if there can be absolutely no confusion
that it’s the only statement within the body, as is the case above. You should avoid single­line
bodies that take up multiple lines:

```cpp
// avoid this ­­ use braces instead
if (foo)
    while (bar)
        if (baz)
            DoTheThing();
```

When in doubt, use braces. It is always okay to use braces, even if the body is trivial:

```cpp
// OK
if (foo)
{
    ++bar;
}
```

## Classes

Classes should generally be structured as follows:

```cpp
class FooBar
{
public:
   // Nested classes, enums, and typedefs go here. These might
   // be private or protected instead of public.
private:
   // private data goes here
protected:
   // protected data goes here
public:
   // public functions goes here
   // simple accessors & mutators go here
protected:
   // protected member functions go here
private:
   // private member functions go here
};
```

This is more of a guideline than a strict rule. Do whatever makes the class readable.

No public data! In other words, you should never have a public member variable. Use
accessors and mutators to manipulate variables from outside of the object.

Prefer initializing member variables in the constructor’s initializer list. It should be formatting like
this:

```cpp
Foo::Foo()
    : m_bar(0)
    , m_baz(0)
{
    //
}
```

This allows you to easily comment out a single line in the initializer list

In-­class initialization is not allowed

```cpp
class Foo
{
    int m_bar = 0; // NO
};
```

The exception is for const static ints that are used to set a member array size:

```cpp
class Foo
{
    static const int sk_barArraySize = 100; // OK
    int mBar[sk_barArraySize];
}
```

## Commenting

You should add comments in the follow circumstances:

- At the top of a function.
  - This comment should explain the purpose of the function.
  - It should describe the inputs, outputs, and any assumptions.
- At the top of a class.
  - Describe the purpose of the class and how it should be used.
  - Explain the main public interface.
  - It’s not necessary to explain simple accessors or mutators.
- For each block of code.
  - There should be a short comment at the top of each block of code describing what that section does.
- For anything that’s not extremely obvious.
  - If you’re doing something tricky, write a big comment explaining it.
  - If you had trouble figuring something out, write out your solution.
  - If there’s a bit of confusing code, write a comment that explains it.
- For any bug fixes or optimizations that weren’t obvious.
  - If you changed something that seems like it should have worked, write a comment explaining what you changed and why.

## Headers

For header guards, there is a choice between this:

```cpp
#pragma once
```

and this:

```cpp
#ifndef FOO_BAR_H
#define FOO_BAR_H

…

#endif // FOO_BAR_H
```

Use the second method since it is more portable, Vadim.

Header files should be self­contained. They should have header guards and include all the
other headers necessary to compile them. In general, try to avoid adding `#include`’s inside
header files where possible. Use forward declarations when you can..

## Misc

Avoid the postfix increment and decrement operators. Always use the prefix version unless
there’s a real reason not too (though there almost never is):

```cpp
// yes
++foo;
­­bar
;
// no
foo++;
bar­­;
```

Use parentheses whenever there’s any confusion over the order of operations. Don’t rely on
other programmers memorizing the operator precedence tables. For example:

```cpp
// NO
if (foo && bar || baz)
    DoTheThing();

// YES
if (foo && (bar || baz))
    DoTheThing();
```

**C++ exceptions should not be used for any reason**

## C++ 11 / 14

The use of C++ 11 and 14 are allowed as long as it compiles with Visual Studio 2013.
Specifically, the following features are highly encouraged:

Range­based for loops:

```cpp
std::vector<GameObject*> gameObjects; // assume this is filled out
for (GameObject* pGameObject : gameObjects)
{
    pGameObject­>Update();
}
```

The auto keyword:

```cpp
auto it = gameObjects.begin();
```

Scoped enums:

```cpp
enum class Foo
{
    BAR,
    BAZ,
};
```

Lambdas:

```cpp
auto lambda = []() { cout << "foo" << endl; };
```

override and final keywords:

```cpp
class Foo
{
public:
    virtual void DoTheThing() = 0;
    virtual void DoSomethingElse() = 0;
};

class Bar : public Foo
{
public:
    virtual void DoTheThing() override;
    virtual void DoSomethingElse() final;
};
```

## C#

Properties should be named like a local variable, except that a prefix is not necessary. If it is an
accessor, it should share the same name as the variable it’s tied to. Note that all the rules for
single­line functions apply to properties. Example:

```cs
public class Foo
{
    private int m_bar = 0;

    public int bar
    {
        // This is a single­statement getter, so it’s okay to be
        // one line:
        get { return m_bar; }
        // Not a single­statement setter, so it must be multiple
        // lines:
        set
        {
            if (value >= 0 && value < 100)
                m_bar = value;
        }
    }
}
```

All functions and variables must have an explicit access modifier rather than relying on the
default of everything being private:

```cs
private int m_foo = 0; // YES
int m_foo = 0; // NO
```

## Lua

Tables that are being used as classes should be named like classes.

Lua has no concept of public or private, but those concepts are still important. Variables and
functions that are prefixed with a leading underscore are considered private and should be
treated as such:

```lua
function Foo:_Bar()
­­    ...
end
```

Since Lua requires the self keyword to access member functions and variables, it is not
necessary to use the ‘m_’ prefix. Likewise, there are no static variables. Global variables
should still be marked with a ‘g_’ prefix.
